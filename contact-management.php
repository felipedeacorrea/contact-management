<?php

/**
 * Plugin Name: Contact Management Plufin
 * Description: Develop a Wordpress plugin to manage contacts
 * Author: Felipe Correa
 * version: 1.0.0
 */

namespace CONTACTMANAGEMENT;

use CONTACTMANAGEMENT\Controllers\Dashboard;
use CONTACTMANAGEMENT\Controllers\People;

require_once __DIR__ . '/vendor/autoload.php';
require_once dirname(__FILE__) . '/class-tgm-plugin-activation.php';

add_action('tgmpa_register', function () {
    $plugins = array(
        array(
            'name' => 'CMB2',
            'slug' => 'cmb2',
            'source' => 'https://downloads.wordpress.org/plugin/cmb2.zip',
            'required' => true,
        )
    );

    $config = array(
        'id' => 'cmb2',
        'default_path' => '',
        'menu' => 'tgmpa-install-plugins',
        'parent_slug' => 'plugins.php',
        'capability' => 'manage_options',
        'has_notices' => true,
        'dismissable' => true,
        'dismiss_msg' => '',
        'is_automatic' => false,
        'message' => '',
        'strings'      => array(
            'page_title'                      => __( 'Instalar plugin obrigatório', 'cmb2' ),
            'menu_title'                      => __( 'Instalar plugins', 'cmb2' ),
            'installing'                      => __( 'Instalar plugin: %s', 'cmb2' ),
            'updating'                        => __( 'Atualizar Plugin: %s', 'cmb2' ),
            'oops'                            => __( 'Something went wrong with the plugin API.', 'cmb2' ),
            'notice_can_install_required'     => _n_noop(
                'Este tema requer o seguinte plugin: %1$s.',
                'Este tema requer os seguintes plugins: %1$s.',
                'cmb2'
            ),
            'notice_can_install_recommended'  => _n_noop(
                'This theme recommends the following plugin: %1$s.',
                'This theme recommends the following plugins: %1$s.',
                'cmb2'
            ),
            'notice_ask_to_update'            => _n_noop(
                'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
                'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
                'cmb2'
            ),
            'notice_ask_to_update_maybe'      => _n_noop(
                'There is an update available for: %1$s.',
                'There are updates available for the following plugins: %1$s.',
                'cmb2'
            ),
            'notice_can_activate_required'    => _n_noop(
                'O seguinte plug-in obrigatório está atualmente inativo: %1$s.',
                'Os seguintes plugins obrigatórios estão atualmente inativo: %1$s.',
                'cmb2'
            ),
            'notice_can_activate_recommended' => _n_noop(
                'The following recommended plugin is currently inactive: %1$s.',
                'The following recommended plugins are currently inactive: %1$s.',
                'cmb2'
            ),
            'install_link'                    => _n_noop(
                'Instalar Plugin',
                'Instalar Plugins',
                'cmb2'
            ),
            'update_link' 					  => _n_noop(
                'Atualizar Plugin',
                'Atualizar Plugins',
                'cmb2'
            ),
            'activate_link'                   => _n_noop(
                'Ativar Plugin',
                'Ativar Plugins',
                'cmb2'
            ),
            'return'                          => __( 'Return to Required Plugins Installer', 'cmb2' ),
            'plugin_activated'                => __( 'Plugin activated successfully.', 'cmb2' ),
            'activated_successfully'          => __( 'The following plugin was activated successfully:', 'cmb2' ),
            'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'cmb2' ),
            'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'cmb2' ),
            'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'cmb2' ),
            'dismiss'                         => __( 'Dismiss this notice', 'cmb2' ),
            'notice_cannot_install_activate'  => __( 'There are one or more required or recommended plugins to install, update or activate.', 'cmb2' ),
            'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'cmb2' ),
            'nag_type'                        => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
        ),
    );

    tgmpa($plugins, $config);
});

add_action('init', function () {
    $contact = new PostType\Contact;
    $people = new PostType\People;
    $dashboard = new Controllers\Dashboard;
    new Controllers\People;

    $contact->register();
    $people->register();
    $dashboard->criarPagina();
});

add_action('rest_api_init', function (){
	$peopleControllers = new Controllers\People;
	$contactControllers = new Controllers\Contact;

	register_rest_route('cm/v1', '/people/novo', [
		'methods' => 'POST',
		'callback' => [$peopleControllers, 'novo'],
		'permission_callback' => function($request){
			return current_user_can('manage_options');
		}
	]);

    register_rest_route('cm/v1', '/people/editar', [
		'methods' => 'POST',
		'callback' => [$peopleControllers, 'editar'],
		'permission_callback' => function($request){
			return current_user_can('manage_options');
		}
	]);

    register_rest_route('cm/v1', '/people/excluir', [
		'methods' => 'POST',
		'callback' => [$peopleControllers, 'excluir'],
		'permission_callback' => function($request){
			return current_user_can('manage_options');
		}
	]);

    register_rest_route('cm/v1', '/contact/novo', [
		'methods' => 'POST',
		'callback' => [$contactControllers, 'novo'],
		'permission_callback' => function($request){
			return current_user_can('manage_options');
		}
	]);

    register_rest_route('cm/v1', '/contact/editar', [
		'methods' => 'POST',
		'callback' => [$contactControllers, 'editar'],
		'permission_callback' => function($request){
			return current_user_can('manage_options');
		}
	]);

    register_rest_route('cm/v1', '/contact/excluir', [
		'methods' => 'POST',
		'callback' => [$contactControllers, 'excluir'],
		'permission_callback' => function($request){
			return current_user_can('manage_options');
		}
	]);
});