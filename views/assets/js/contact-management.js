(function ($) {
    $(document).ready(function () {
        $('#exampleFormControlSelect1').select2({
            dropdownParent: $('#modalContato')
        });

        $('#exampleFormControlSelect2').select2({
            dropdownParent: $('#modalEditar')
        });

        $('.add-contato').click(function(){
            $('#id_pessoa').val($(this).data('id'))
        })

        $('.editar-contato').click(function(){
            $('#modalEditar #telefone').val($(this).data('numero'))
            $('#modalEditar #id_contato').val($(this).data('id_contato'))
            $('#modalEditar #id_people').val($(this).data('id_people'))
            $('#modalEditar #exampleFormControlSelect2').val($(this).data('codigo'))
            $('#modalEditar #exampleFormControlSelect2').trigger('change');
        })
    });
})(jQuery);