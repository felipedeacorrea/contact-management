(function ($) {
    $(document).ready(function () {
        $("form.ajaxDados").submit(function (e) {
            e.preventDefault();

            var form = $(this),
                base_url = $('#base_url').val(),
                nonce = $('#nonce').val();

            form.ajaxSubmit({
                url: form.attr("action"),
                type: "POST",
                dataType: "json",
                beforeSend: function() {},
                success: function (response) {
                  notificacao(response);

                  action(response.action, 2)
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Houve um erro na requisição")
                }
            });
        });

        $('.ajaxLink').click(function (e) {
            e.preventDefault();
            var $url = $(this).attr('href'),
                post_id = $(this).data('id');

            $.ajax({
                url: $url,
                type: "POST",
                dataType: "json",
                data: {
                    id: post_id,
                },
                success: function (response) {

                    notificacao(response);

                    action(response.action, 2)

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Houve um erro na requisição")
                }
            });

        });
    });

    // gera notificação com o sweet alert
    function notificacao(response) {

        var title = "Sucesso"

        if (response.status == "error") {
            title = "Erro";
        }

        swal({
            title: title,
            text: response.msg,
            icon: response.status,
            button: "Ok",
        });
    }

    // gera a ação de acordo com o tipo e o time
    function action(action, time) {
        switch (action) {
            case 'reload':
                window.setTimeout(function () {
                    window.location.reload();
                }, time * 1000);
                break;

            /* Padrão */
            default:
                console.log('Ação não reconhecida');
        }
    }

})(jQuery);
