<?php

namespace CONTACTMANAGEMENT\Models;

use \CONTACTMANAGEMENT\PostType\Contact as ContactPostType;

class Contact
{
    private $id;
    private $prefixo_post_meta;
    private $titulo;
    private $id_people;
    private $codigo;
    private $numero;

    public function __construct($wp_post)
    {
        $this->id                   = $wp_post->ID ? $wp_post->ID : null;
        $this->titulo               = $wp_post->post_title ? $wp_post->post_title : null;
        $this->prefixo_post_meta    = ContactPostType::getPrefixPostMeta();
        $this->id_people            = get_post_meta($wp_post->ID, $this->prefixo_post_meta . 'id_people', true);
        $this->codigo               = get_post_meta($wp_post->ID, $this->prefixo_post_meta . 'codigo', true);
        $this->numero               = get_post_meta($wp_post->ID, $this->prefixo_post_meta . 'numero', true);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function getPrefixoPostMeta()
    {
        return $this->prefixo_post_meta;
    }

    public function getTitulo()
    {
        return $this->titulo;
    }

    public function getIDPeople()
    {
        return $this->id_people;
    }

    public function getCodigo()
    {
        return $this->codigo;
    }

    public function getNumero()
    {
        return $this->numero;
    }
}
