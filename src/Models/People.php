<?php

namespace CONTACTMANAGEMENT\Models;

use \CONTACTMANAGEMENT\PostType\People as PeoplePostType;
use CONTACTMANAGEMENT\Providers\Contact as ContactProvider;

class People
{
    private $id;
    private $prefixo_post_meta;
    private $post_meta;
    private $titulo;
    private $nome;
    private $email;
    private $contato;

    public function __construct($wp_post)
    {
        $this->id                   = $wp_post->ID ? $wp_post->ID : null;
        $this->titulo               = $wp_post->post_title ? $wp_post->post_title : null;
        $this->prefixo_post_meta    = PeoplePostType::getPrefixPostMeta();
        $this->post_meta            = get_post_meta($wp_post->ID, '', true);
        $this->nome                 = get_post_meta($wp_post->ID, $this->prefixo_post_meta . 'name', true);
        $this->email                = get_post_meta($wp_post->ID, $this->prefixo_post_meta . 'email', true);
        $this->contato              = ContactProvider::getByPessoa($wp_post->ID);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function getPrefixoPostMeta()
    {
        return $this->prefixo_post_meta;
    }

    public function getPostMeta()
    {
        return $this->post_meta;
    }

    public function getTitulo()
    {
        return $this->titulo;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getContato()
    {
        return $this->contato;
    }
}