<?php

namespace CONTACTMANAGEMENT\Providers;

use CONTACTMANAGEMENT\Models\Contact as ContactModel;
use \CONTACTMANAGEMENT\PostType\Contact as ContactPostType;

class Contact
{
    /**
     * Get todos os contatos
     *
     * @param integer $number
     * @return void
     */
    public static function getTodos($number = -1)
    {
        $wp_posts = get_posts([
            'post_type' => 'contacts',
            'numberposts' => $number,
            'post_status' => 'publish'
        ]);

        $dados = array_map(function ($wp_post) {
            return new ContactModel($wp_post);
        }, $wp_posts);

        return $dados;
    }

    /**
     * Get os contato by id pessoa
     *
     * @param [type] $pessoa
     * @return void
     */
    public static function getByPessoa($pessoa)
    {

        $wp_posts = get_posts([
            'post_type' => 'contacts',
            'post_status' => 'publish',
            'meta_query' => array(
                array(
                    'key'   => ContactPostType::getPrefixPostMeta() . 'id_people',
                    'value' => $pessoa,
                )
            )
        ]);

        $dados = array_map(function ($wp_post) {
            return new ContactModel($wp_post);
        }, $wp_posts);

        return $dados;
    }

    /**
     * get contato by ID
     *
     * @param [type] $id
     * @return void
     */
    public static function getById($id)
    {
        $wp_posts = get_posts([
            'post_type' => 'contacts',
            'post__in' => array($id),
        ]);

        $dados = array_map(function ($wp_post) {
            return new ContactModel($wp_post);
        }, $wp_posts);

        return $dados;
    }


}