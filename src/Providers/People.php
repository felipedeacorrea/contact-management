<?php

namespace CONTACTMANAGEMENT\Providers;

use CONTACTMANAGEMENT\Models\People as PeopleModel;
use \CONTACTMANAGEMENT\PostType\People as PeoplePostType;

class People
{
    /**
     * Get todas as pessoas
     *
     * @param integer $number
     * @return void
     */
    public static function getTodos($number = -1)
    {
        $wp_posts = get_posts([
            'post_type' => 'peoples',
            'numberposts' => $number,
            'post_status' => 'publish'
        ]);

        $dados = array_map(function ($wp_post) {
            return new PeopleModel($wp_post);
        }, $wp_posts);

        return $dados;
    }

    /**
     * get pessoa by email
     *
     * @param [type] $email
     * @return void
     */
    public static function getByEmail($email)
    {
        $wp_posts = get_posts([
            'post_type' => 'peoples',
            'post_status' => 'publish',
            'meta_query' => array(
                array(
                    'key'   => PeoplePostType::getPrefixPostMeta() . 'email',
                    'value' => $email,
                )
            )
        ]);

        $dados = array_map(function ($wp_post) {
            return new PeopleModel($wp_post);
        }, $wp_posts);

        return $dados;
    }

    /**
     * Get pessoa by id
     *
     * @param [type] $id
     * @return void
     */
    public static function getById($id)
    {
        $wp_posts = get_posts([
            'post_type' => 'peoples',
            'post__in' => array($id),
        ]);

        $dados = array_map(function ($wp_post) {
            return new PeopleModel($wp_post);
        }, $wp_posts);

        return $dados;
    }


}