<?php

namespace CONTACTMANAGEMENT\PostType;

class Contact
{

    public function getPrefixPostMeta()
    {
        return 'contact_';
    }

    public function register()
    {
        $text_domain = 'template_name';

        register_post_type('contacts', [
            'labels' => [
                'name' => __('Contatos', $text_domain),
                'singular_name' => __('Contato', $text_domain),
                'add_new' => _x('Adicionar novo', $text_domain),
                'all_items' => _x('Todos os Contatos', $text_domain),
                'add_new_item' => _x('Adicionar novo', $text_domain),
                'edit_item' => _x('Editar', $text_domain),
                'new_item' => _x('Novo', $text_domain),
                'view_item' => _x('Ver', $text_domain),
                'search_items' => _x('Procurar', $text_domain),
                'not_found' => _x('Nada encontrado', $text_domain),
                'not_found_in_trash' => _x('Nada encontrado na lixeira', $text_domain),
                'parent_item_colon' => null,
                'menu_name' => _x('Contatos', $text_domain),
            ],
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'exclude_from_search' => true,
            'show_in_nav_menus' => true,
            'show_in_menu' => true,
            'show_in_admin_bar' => true,
            'menu_position' => 20,
            'menu_icon' => 'dashicons-megaphone',
            'capability_type' => 'post',
            'supports' => ['title'],
            'rewrite' => ['slug' => 'contacts', 'with_front' => false],
            'has_archive' => true
        ]);

        add_action('cmb2_admin_init', function () {
            $this->registerPostMeta();
        });
    }

    private function registerPostMeta()
    {
        $prefixo = $this->getPrefixPostMeta();

        $cmb = new_cmb2_box([
            'id' => $prefixo . '_metabox',
            'title' => 'Dados do contato',
            'object_types' => ['contacts']
        ]);

        $cmb->add_field(array(
            'name'    => 'Id Pessoa',
            'id'      => $prefixo . 'id_people',
            'type'    => 'text',
            'attributes' => array(
                'readonly' => 'readonly',
                'data-codeeditor' => json_encode(array(
                    'codemirror' => array(
                        'mode' => 'css',
                        'readOnly' => 'nocursor',
                    ),
                )),
            ),
        ));

        $cmb->add_field(array(
            'name'    => 'Codigo de area',
            'id'      => $prefixo . 'codigo',
            'type'    => 'text',
        ));

        $cmb->add_field(array(
            'name' => 'Número',
            'id'   => $prefixo . 'numero',
            'type'    => 'text',
        ));
    }
}
