<?php

namespace CONTACTMANAGEMENT\PostType;

class People
{

    public function getPrefixPostMeta()
    {
        return 'people_';
    }

    public function register()
    {
        $text_domain = 'template_name';

        register_post_type('peoples', [
            'labels' => [
                'name' => __('Pessoas', $text_domain),
                'singular_name' => __('Pessoa', $text_domain),
                'add_new' => _x('Adicionar novo', $text_domain),
                'all_items' => _x('Todas as Pessoas', $text_domain),
                'add_new_item' => _x('Adicionar nova', $text_domain),
                'edit_item' => _x('Editar', $text_domain),
                'new_item' => _x('Novo', $text_domain),
                'view_item' => _x('Ver', $text_domain),
                'search_items' => _x('Procurar', $text_domain),
                'not_found' => _x('Nada encontrado', $text_domain),
                'not_found_in_trash' => _x('Nada encontrado na lixeira', $text_domain),
                'parent_item_colon' => null,
                'menu_name' => _x('Pessoas', $text_domain),
            ],
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'exclude_from_search' => true,
            'show_in_nav_menus' => true,
            'show_in_menu' => true,
            'show_in_admin_bar' => true,
            'menu_position' => 20,
            'menu_icon' => 'dashicons-universal-access-alt',
            'capability_type' => 'post',
            'supports' => ['title'],
            'rewrite' => ['slug' => 'peoples', 'with_front' => false],
            'has_archive' => true
        ]);

        add_action('cmb2_admin_init', function () {
            $this->registerPostMeta();
        });
    }

    private function registerPostMeta()
    {
        $prefixo = $this->getPrefixPostMeta();

        $cmb = new_cmb2_box([
            'id' => $prefixo . '_metabox',
            'title' => 'Detalhes da pessoa',
            'object_types' => ['peoples']
        ]);

        $cmb->add_field( array(
            'name'    => 'Nome',
            'id'      => $prefixo . 'name',
            'type'    => 'text',
        ) );

        $cmb->add_field( array(
            'name' => 'E-mail',
            'id'   => $prefixo . 'email',
            'type' => 'text_email',
        ) );
    }
    
}
