<?php

namespace CONTACTMANAGEMENT\Controllers;

use \CONTACTMANAGEMENT\Helpers\TwigViewer;
use CONTACTMANAGEMENT\Providers\People as PeopleProvider;
use CONTACTMANAGEMENT\Providers\Contact as ContactProvider;

class Dashboard extends BaseController
{
    /**
     * Criar pagina principal no admin
     *
     * @return void
     */
    public function criarPagina()
    {
        add_action('admin_menu', function () {
            $page = add_menu_page(
                'Gestão de contato',
                'Gestão de contato',
                'manage_options',
                'contact-management',
                [$this, 'contactManamentCallback'],
                plugins_url('contact-management/views/assets/img/social.png'),
                6
            );

            add_action("load-$page", function () {
                add_action("admin_enqueue_scripts", [$this, 'enqueueScripts']);
            });
        });
    }

    /**
     * callback da paginda adminitrativa
     *
     * @return void
     */
    public function contactManamentCallback()
    {
        if (!current_user_can('manage_options')) {
            return;
        }

        $args = [
            'base_url' => home_url(),
            'nonce' => wp_create_nonce('wp_rest')
        ];

        if (isset($_GET['action']) && $_GET['action'] == 'novo') {
            $layout = "novo.html";
        } elseif (isset($_GET['action']) && $_GET['action'] == 'editar') {
            $id = $_GET['id'];
            if ($id) {
                $pessoa = PeopleProvider::getById($id);
                if ($pessoa) {
                    $args['nome'] = $pessoa[0]->getNome();
                    $args['email'] = $pessoa[0]->getEmail();
                    $args['id'] = $id;
                }
                $layout = "editar.html";
            } else {
                $layout = "error.html";
            }
        } else {
            $args['pessoas'] = PeopleProvider::getTodos();
            $args['country'] = $this->getCountries();
            $layout = "index.html";
        }

        echo TwigViewer::render($layout, $args);
    }

    /**
     * Carregando os assets
     *
     * @return void
     */
    public function enqueueScripts()
    {
        wp_enqueue_style('bootstrap', '//stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css', [], false);
        wp_enqueue_style('select2.css', '//cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css', [], false);
        wp_enqueue_style('contact-management.css', "{$this->assetsBaseURL}/css/contact-management.css", [], false);
        wp_enqueue_script('bootstrap.min.js', '//stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', ['jquery'], false, true);
        wp_enqueue_script('sweetalert', '//unpkg.com/sweetalert/dist/sweetalert.min.js', ['jquery'], false, false);
        wp_enqueue_script('select2.js', '//cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js', ['jquery'], false, false);
        wp_enqueue_script('jquery-form.js', "{$this->assetsBaseURL}/js/jquery-form/jquery.form.js", ['jquery'], false, true);
        wp_enqueue_script('ajax-dados.js', "{$this->assetsBaseURL}/js/ajax-dados.js", ['jquery'], false, true);
        wp_enqueue_script('contact-management.js', "{$this->assetsBaseURL}/js/contact-management.js", ['jquery'], false, true);
    }

    /**
     * get das countries
     *
     * @return void
     */
    private function getCountries(){
        $curl = curl_init();
        
        curl_setopt_array($curl, [
          CURLOPT_URL => "https://restcountries.com/v3.1/all",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_POSTFIELDS => "",
          CURLOPT_HTTPHEADER => [
            "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9vdXNld2ViLmRpZ2l0YWwiLCJhdWQiOiJodHRwOlwvXC9vdXNld2ViLmRpZ2l0YWwiLCJpYXQiOjEzNTY5OTk1MjQsIm5iZiI6MTM1NzAwMDAwMH0.Gfx2W4b7um-PUGsv3XBdcvQLhWNuekXHvNWFxt5D0a8"
          ],
        ]);
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
          return false;
        } else {
          $dados = json_decode($response);
        }

        $arrPronto = [];
        if($dados){
            foreach ($dados as $key => $value) {
                $arrPronto[$key] = [
                    'nome' => $value->name->common,
                    'cod' => $value->idd->root . $value->idd->suffixes[0],
                ];
            }
        }

        return $arrPronto;
    }
}
