<?php

namespace CONTACTMANAGEMENT\Controllers;

use CONTACTMANAGEMENT\Providers\Contact as ContactProvider;

class Contact extends BaseController
{
	/**
	 * Enpoint responsavel por criar uma nova pessoa
	 *
	 * @return void
	 */
	public function novo()
	{
		$post = $_POST;

		$args = [
			'id_people' => $post['id_pessoa'],
			'codigo' => $post['codigo'],
			'numero' => $post['telefone']
		];

		if(empty($args['codigo']) || empty($args['numero']) || empty($args['id_people'])){
			return $this->printJSON('danger', 'é obrigatorio o envio do codigo, numero e id_people', $post, '');
		}

		$metaDados = $this->agrupaDados($args);
		$postArray = $this->agrupaDadosPostType($args, $metaDados);

		$post_id = wp_insert_post($postArray);

		if(!is_wp_error($post_id)){
			$type = "success";
			$msg = "Contato criado com sucesso!";
			$action = "reload";
		}else{
			$type = "error";
			$msg = $post_id->get_error_message();
			$action = null;
		}

		return $this->printJSON($type, $msg, $post_id, $action);
	}

	/**
	 * Endpoint responsavel por editar uma pessoa
	 *
	 * @return void
	 */
	public function editar()
	{
		$post = $_POST;

		$args = [
			'id_people' => $post['id_people'],
			'codigo' => $post['codigo'],
			'numero' => $post['telefone'],
			'id' => $post['id_contato'] 
		];

		if(empty($args['codigo'] || $args['numero'])){
			return $this->printJSON('danger', 'é obrigatorio o envio do codigo e numero', $post, '');
		}

		$metaDados = $this->agrupaDados($args);
		$postArray = $this->agrupaDadosPostType($args, $metaDados);

		$post_id = wp_update_post($postArray);

		if(!is_wp_error($post_id)){
			$type = "success";
			$msg = "Contato editado com sucesso!";
			$action = "reload";
		}else{
			$type = "error";
			$msg = $post_id->get_error_message();
			$action = null;
		}

		return $this->printJSON($type, $msg, $post_id, $action);
	}

	public function excluir()
	{
		$dados = [
			'post_status' => 'draft',
			'post_type' => 'contacts',
			'ID' => $_POST['id']
		];

		$post_id = wp_update_post($dados);

		if(!is_wp_error($post_id)){
			$type = "success";
			$msg = "Contato excluido com sucesso!";
			$action = "reload";
		}else{
			$type = "error";
			$msg = $post_id->get_error_message();
			$action = null;
		}

		return $this->printJSON($type, $msg, $post_id, $action);
	}

	/**
	 * Agrupa metadados
	 *
	 * @param [type] $args
	 * @return void
	 */
	private function agrupaDados ($args){
		return [
			'contact_id_people' => $args['id_people'],
			'contact_codigo' => $args['codigo'],
			'contact_numero' => $args['numero']
		];
	}

	/**
	 * Agrupa dados do posttype
	 *
	 * @param [type] $args
	 * @param [type] $metaDados
	 * @return void
	 */
	private function agrupaDadosPostType ($args, $metaDados){
		$dados = [
			'post_title' => "{$args['id_people']} - {$args['codigo']} {$args['numero']}",
			'post_status' => 'publish',
			'post_type' => 'contacts',
			'meta_input' => $metaDados
		];

		if($args['id']){
			$dados['ID'] = $args['id'];
		}

		return $dados;
	}
}
