<?php

namespace CONTACTMANAGEMENT\Controllers;

abstract class BaseController
{
    protected $baseURL = WP_PLUGIN_URL . '/contact-management';
    protected $assetsBaseURL = WP_PLUGIN_URL . '/contact-management/views/assets';

	public function printJSON(string $status, string $msg, $data, $action)
	{
		echo json_encode([
			'status' => $status,
			'msg' => $msg,
			'data' => $data,
			'action' => $action
		]);
	}
}
