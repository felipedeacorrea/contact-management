<?php

namespace CONTACTMANAGEMENT\Controllers;

use \CONTACTMANAGEMENT\Helpers\TwigViewer;
use CONTACTMANAGEMENT\Providers\People as PeopleProvider;
class People extends BaseController
{
	public function __construct()
    {
        add_shortcode('lista_pessoas', [$this, 'getPessoas']);
    }

	public function getPessoas()
    {
        ob_start();

		wp_enqueue_style('bootstrap', '//stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css', [], false);
        wp_enqueue_script('bootstrap.min.js', '//stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', ['jquery'], false, true);

        $args = [
            'pessoas' => PeopleProvider::getTodos()
        ];

        echo TwigViewer::render("lista-pessoas.html", $args);

        return ob_get_clean();
    }

	/**
	 * Enpoint responsavel por criar uma nova pessoa
	 *
	 * @return void
	 */
	public function novo()
	{
		$post = $_POST;

		$args = [
			'nome' => $post['nome'],
			'email' => $post['email']
		];

		if(empty($args['email'] || $args['nome'])){
			return $this->printJSON('danger', 'é obrigatorio o envio do nome e email', $post, '');
		}

		if($this->checkUserByEmail($args['email'])){
			return $this->printJSON('warning', 'Já temos usuario cadastrado com esse e-mail', $post, '');
		}

		$metaDados = $this->agrupaDados($args);
		$postArray = $this->agrupaDadosPostType($args, $metaDados);

		$post_id = wp_insert_post($postArray);

		if(!is_wp_error($post_id)){
			$type = "success";
			$msg = "Pessoa criada com sucesso!";
			$action = "reload";
		}else{
			$type = "error";
			$msg = $post_id->get_error_message();
			$action = null;
		}

		return $this->printJSON($type, $msg, $post_id, $action);
	}

	/**
	 * Endpoint responsavel por editar uma pessoa
	 *
	 * @return void
	 */
	public function editar()
	{
		$post = $_POST;

		$args = [
			'nome' => $post['nome'],
			'email' => $post['email'],
			'id' => $post['id']
		];

		if(empty($args['email'] || $args['nome'])){
			return $this->printJSON('danger', 'é obrigatorio o envio do nome e email', $post, '');
		}

		if($this->checkUserByEmailAndID($args['email'], $args['id'])){
			return $this->printJSON('warning', 'Já temos usuario cadastrado com esse e-mail', $post, '');
		}

		$metaDados = $this->agrupaDados($args);
		$postArray = $this->agrupaDadosPostType($args, $metaDados);

		$post_id = wp_update_post($postArray);

		if(!is_wp_error($post_id)){
			$type = "success";
			$msg = "Pessoa alterada com sucesso!";
			$action = "reload";
		}else{
			$type = "error";
			$msg = $post_id->get_error_message();
			$action = null;
		}

		return $this->printJSON($type, $msg, $post_id, $action);
	}

	/**
	 * Excluir uma pessoa
	 *
	 * @return void
	 */
	public function excluir()
	{
		$dados = [
			'post_status' => 'draft',
			'post_type' => 'peoples',
			'ID' => $_POST['id']
		];

		$post_id = wp_update_post($dados);

		if(!is_wp_error($post_id)){
			$type = "success";
			$msg = "Pessoa excluida com sucesso!";
			$action = "reload";
		}else{
			$type = "error";
			$msg = $post_id->get_error_message();
			$action = null;
		}

		return $this->printJSON($type, $msg, $post_id, $action);
	}

	/**
	 * Agrupa metadados
	 *
	 * @param [type] $args
	 * @return void
	 */
	private function agrupaDados ($args){
		return [
			'people_name' => $args['nome'],
			'people_email' => $args['email']
		];
	}

	/**
	 * Checa se o usuario existe
	 *
	 * @param [type] $email
	 * @return void
	 */
	private function checkUserByEmail ($email){
		$pessoa = PeopleProvider::getByEmail($email);
		
		if(empty($pessoa)){
			return false;
		}else{
			return true;
		}
	}
	
	/**
	 * Checa se o usuario existe e compara com o ID
	 *
	 * @param [type] $email
	 * @param [type] $id
	 * @return void
	 */
	private function checkUserByEmailAndID ($email, $id){
		$pessoa = PeopleProvider::getByEmail($email);
		
		if(empty($pessoa)){
			return false;
		}elseif($pessoa[0]->getId() == $id){
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Agrupa dados do posttype
	 *
	 * @param [type] $args
	 * @param [type] $metaDados
	 * @return void
	 */
	private function agrupaDadosPostType ($args, $metaDados){
		$dados = [
			'post_title' => "{$args['nome']} - {$args['email']}",
			'post_status' => 'publish',
			'post_type' => 'peoples',
			'meta_input' => $metaDados
		];

		if($args['id']){
			$dados['ID'] = $args['id'];
		}

		return $dados;
	}
}
